#!/usr/bin/env bash

./experiments/scripts/faster_rcnn_tless_test.sh 0 ZF $1 ~/data/frcnn/output/faster_rcnn_end2end/t-less_v2_$2/zf_faster_rcnn_iter_70000.caffemodel
./experiments/scripts/faster_rcnn_tless_test.sh 0 ZF $2 ~/data/frcnn/output/faster_rcnn_end2end/t-less_v2_$1/zf_faster_rcnn_iter_70000.caffemodel

./experiments/scripts/faster_rcnn_tless_test.sh 0 ZF scene_all_upper ~/data/frcnn/output/faster_rcnn_end2end/t-less_v2_$1/zf_faster_rcnn_iter_70000.caffemodel
./experiments/scripts/faster_rcnn_tless_test.sh 0 ZF scene_all_upper ~/data/frcnn/output/faster_rcnn_end2end/t-less_v2_$2/zf_faster_rcnn_iter_70000.caffemodel

python ~/transdataform/frcnn/plot_pr.py \
    ~/data/frcnn/output/plot_pr_traintrain_$1_$2.png \
    ~/data/frcnn/output/faster_rcnn_end2end/t-less_v2_$1/zf_faster_rcnn_iter_70000_t-less_v2_$1 \
    ~/data/frcnn/output/faster_rcnn_end2end/t-less_v2_$1/zf_faster_rcnn_iter_70000_t-less_v2_$2 \
    ~/data/frcnn/output/faster_rcnn_end2end/t-less_v2_$2/zf_faster_rcnn_iter_70000_t-less_v2_$1 \
    ~/data/frcnn/output/faster_rcnn_end2end/t-less_v2_$2/zf_faster_rcnn_iter_70000_t-less_v2_$2

python ~/transdataform/frcnn/plot_pr.py \
    ~/data/frcnn/output/plot_pr_trainscene_$1_$2.png \
    ~/data/frcnn/output/faster_rcnn_end2end/t-less_v2_scene_all_upper/zf_faster_rcnn_iter_70000_t-less_v2_$1 \
    ~/data/frcnn/output/faster_rcnn_end2end/t-less_v2_scene_all_upper/zf_faster_rcnn_iter_70000_t-less_v2_$2
