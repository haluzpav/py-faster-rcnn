#!/usr/bin/env bash

set -x
set -e

export PYTHONUNBUFFERED="True"

MODEL=$1
IMDB=$2
IMDB_TRAIN=$3
IMG_TYPES=$4
NET=$5

array=( $@ )
len=${#array[@]}
EXTRA_ARGS=${array[@]:6:$len}
EXTRA_ARGS_SLUG=${EXTRA_ARGS// /_}

LOG=~/data/frcnn/output/${MODEL}_${IMG_TYPES}/${IMDB_TRAIN}/${IMDB}_${NET}/test_${EXTRA_ARGS_SLUG}.`date +'%Y-%m-%d_%H-%M-%S'`.log
mkdir -p `dirname "$LOG"`
exec &> >(tee -a "$LOG")
echo Logging output to "$LOG"

time python ./tools/test_net.py \
  --def models/${MODEL}/test.prototxt \
  --net ~/data/frcnn/output/${MODEL}_${IMG_TYPES}/${IMDB_TRAIN}/${NET}.caffemodel \
  --imdb hinter_${IMDB} \
  --cfg experiments/cfgs/faster_rcnn_end2end_hinter.yml \
  --img_types ${IMG_TYPES} \
  ${EXTRA_ARGS}
