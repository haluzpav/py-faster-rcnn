#!/usr/bin/env bash

set -x
set -e

export PYTHONUNBUFFERED="True"

MODEL=$1 # tless_ch4_00
IMDB=$2 # train_primesense_s0.80_r000
IMG_TYPES=$3 # rgb_d_im_m_gsx
NET_START=$4 # root = output
ITERS=$5 # 70000

array=( $@ )
len=${#array[@]}
EXTRA_ARGS=${array[@]:6:$len}
EXTRA_ARGS_SLUG=${EXTRA_ARGS// /_}

LOG=~/data/frcnn/output/${MODEL}_${IMG_TYPES}/${IMDB}/train_"${EXTRA_ARGS_SLUG}".`date +'%Y-%m-%d_%H-%M-%S'`.log
mkdir -p `dirname "$LOG"`
exec &> >(tee -a "$LOG")
echo Logging output to "$LOG"

time python ./tools/train_net.py \
  --solver models/${MODEL}/solver.prototxt \
  --weights ~/data/frcnn/output/${NET_START}.caffemodel \
  --imdb hinter_${IMDB} \
  --iters ${ITERS} \
  --cfg experiments/cfgs/faster_rcnn_end2end_hinter.yml \
  --img_types ${IMG_TYPES} \
  ${EXTRA_ARGS}

python ~/transdataform/frcnn/plot_losses.py "$LOG"

set +x
NET_FINAL=`grep -B 1 "done solving" ${LOG} | grep "Wrote snapshot" | awk '{print $4}'`
set -x

#time python ./tools/test_net.py \
#  --def models/${MODEL}/test.prototxt \
#  --net ${NET_FINAL} \
#  --imdb hinter_${IMDB} \
#  --cfg experiments/cfgs/faster_rcnn_end2end_hinter.yml \
#  --img_types ${IMG_TYPES} \
#  ${EXTRA_ARGS}
