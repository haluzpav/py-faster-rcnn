#!/usr/bin/env bash


#python ~/transdataform/frcnn/extend_net.py normal \
#    ~/frcnn/models/hinter_ZF_ch3_00/test.prototxt \
#    ~/frcnn/models/hinter_ZF_ch6_00/test.prototxt \
#    ~/data/frcnn/output/tless_ZF_ch3_00_rgb/ZF.v2.caffemodel \
#    ~/data/frcnn/output/hinter_ZF_ch6_00_he_rgb_nxyz/imagenet_normal.caffemodel


#python ~/transdataform/frcnn/switch_net.py normal \
#    ~/frcnn/models/hinter_ZF_ch3_00/test.prototxt \
#    ~/frcnn/models/hinter_ZF_ch4_00/test.prototxt \
#    ~/data/frcnn/output/tless_ZF_ch3_00_tr_rgb/ZF.v2.caffemodel \
#    ~/data/frcnn/output/hinter_ZF_ch4_00_he_rgb_d/imagenet_normal_first.caffemodel \
#    first


#python ~/transdataform/frcnn/switch_net.py normal \
#    ~/frcnn/models/hinter_ZF_ch3_00/test.prototxt \
#    ~/frcnn/models/hinter_ZF_ch6_00/test.prototxt \
#    ~/data/frcnn/output/tless_ZF_ch3_00_tr_rgb/ZF.v2.caffemodel \
#    ~/data/frcnn/output/hinter_ZF_ch6_00_he_rgb_nxyz/imagenet_normal_first.caffemodel \
#    first


#python ~/transdataform/tless/extend_depth.py test --inpaint median --derivate normals_xyz --thresholds_normals 18000 30 --dataset hinter
#python ~/transdataform/tless/extend_depth.py test --inpaint median --dataset hinter --singlethread
#python ~/transdataform/tless/extend_depth.py test --inpaint median --dataset hinter
#python ~/transdataform/tless/extend_depth.py test --derivate normals_xyz2 --thresholds_normals 180000 30 --dataset hinter
#python ~/transdataform/tless/extend_depth.py test --grayscale --dataset hinter


#python ~/transdataform/tless/lists_split.py test_all test --dataset hinter


#bash experiments/scripts/frcnn_hinter_train.sh hinter_ZF_ch3_00 test_all_50train he_rgb tless_ZF_ch3_00_rgb/ZF.v2 100000

#bash experiments/scripts/frcnn_hinter_train.sh hinter_ZF_ch3_00 test_all_50train he_rgb hinter_ZF_ch3_00_he_nxyz/imagenet_normal_first 170000
#mv ~/data/frcnn/output/hinter_ZF_ch3_00_he_rgb/test_all_50train ~/data/frcnn/output/hinter_ZF_ch3_00_he_rgb/test_all_50train_FIRST
#bash experiments/scripts/frcnn_hinter_train.sh hinter_ZF_ch3_00 test_all_50train he_rgb hinter_ZF_ch3_00_he_rgb/imagenet_normal_convs 170000
#mv ~/data/frcnn/output/hinter_ZF_ch3_00_he_rgb/test_all_50train ~/data/frcnn/output/hinter_ZF_ch3_00_he_rgb/test_all_50train_CONVS
#bash experiments/scripts/frcnn_hinter_train.sh hinter_ZF_ch3_00 test_all_50train he_rgb hinter_ZF_ch3_00_he_rgb/normal_all 170000
#mv ~/data/frcnn/output/hinter_ZF_ch3_00_he_rgb/test_all_50train ~/data/frcnn/output/hinter_ZF_ch3_00_he_rgb/test_all_50train_ALL

#bash experiments/scripts/frcnn_hinter_train.sh hinter_ZF_ch3_00 test_all_50train he_nxyz2 hinter_ZF_ch3_00_he_nxyz/imagenet_normal_first 100000
#mv ~/data/frcnn/output/hinter_ZF_ch3_00_he_nxyz2/test_all_50train ~/data/frcnn/output/hinter_ZF_ch3_00_he_nxyz2/test_all_50train_FIRST

#bash experiments/scripts/frcnn_hinter_train.sh hinter_ZF_ch4_00 test_all_50train he_rgb_d hinter_ZF_ch4_00_he_rgb_d/imagenet_normal_first 100000
#mv ~/data/frcnn/output/hinter_ZF_ch4_00_he_rgb_d/test_all_50train ~/data/frcnn/output/hinter_ZF_ch4_00_he_rgb_d/test_all_50train_FIRST
#bash experiments/scripts/frcnn_hinter_train.sh hinter_ZF_ch4_00 test_all_50train he_rgb_im hinter_ZF_ch4_00_he_rgb_d/imagenet_normal_first 100000
#mv ~/data/frcnn/output/hinter_ZF_ch4_00_he_rgb_im/test_all_50train ~/data/frcnn/output/hinter_ZF_ch4_00_he_rgb_im/test_all_50train_FIRST

#bash experiments/scripts/frcnn_hinter_train.sh hinter_ZF_ch4_00 test_all_50train he_rgb_im hinter_ZF_ch4_00_he_rgb_d/imagenet_rgbmean 100000
#mv ~/data/frcnn/output/hinter_ZF_ch4_00_he_rgb_im/test_all_50train ~/data/frcnn/output/hinter_ZF_ch4_00_he_rgb_im/test_all_50train_RGBMEAN

bash experiments/scripts/frcnn_hinter_train.sh hinter_ZF_ch1_00 test_all_50train he_im hinter_ZF_ch1_00_he_d/imagenet_rgbmean 100000
mv ~/data/frcnn/output/hinter_ZF_ch1_00_he_im/test_all_50train ~/data/frcnn/output/hinter_ZF_ch1_00_he_im/test_all_50train_RGBMEAN

#bash experiments/scripts/frcnn_hinter_train.sh hinter_ZF_ch6_00 test_all_50train he_rgb_nxyz2 hinter_ZF_ch6_00_he_rgb_nxyz/imagenet_normal_first 100000
#mv ~/data/frcnn/output/hinter_ZF_ch6_00_he_rgb_nxyz2/test_all_50train ~/data/frcnn/output/hinter_ZF_ch6_00_he_rgb_nxyz2/test_all_50train_FIRST

#bash experiments/scripts/frcnn_hinter_train.sh hinter_ZF_ch4_00 test_all_50train he_rgb_d hinter_ZF_ch4_00_he_rgb_d/imagenet_normal 100000
#bash experiments/scripts/frcnn_hinter_train.sh hinter_ZF_ch4_00 test_all_50train he_rgb_im hinter_ZF_ch4_00_he_rgb_d/imagenet_normal 100000
#bash experiments/scripts/frcnn_hinter_train.sh hinter_ZF_ch1_00 test_all_50train he_d hinter_ZF_ch1_00_he_d/imagenet_normal 100000
#bash experiments/scripts/frcnn_hinter_train.sh hinter_ZF_ch1_00 test_all_50train he_im hinter_ZF_ch1_00_he_d/imagenet_normal 100000
#bash experiments/scripts/frcnn_hinter_train.sh hinter_ZF_ch3_00 test_all_50train he_nxyz tless_ZF_ch3_00_rgb/ZF.v2 100000
#bash experiments/scripts/frcnn_hinter_train.sh hinter_ZF_ch6_00 test_all_50train he_rgb_nxyz hinter_ZF_ch6_00_he_rgb_nxyz/imagenet_normal 100000
#bash experiments/scripts/frcnn_hinter_train.sh hinter_ZF_ch1_00 test_all_50train he_g hinter_ZF_ch1_00_he_d/imagenet_normal 100000


#bash experiments/scripts/frcnn_hinter_test.sh hinter_ZF_ch3_00 test_all_50test test_all_50train he_rgb iter_100000
#bash experiments/scripts/frcnn_hinter_test.sh hinter_ZF_ch3_00 test_all_50test test_all_50train_FIRST he_rgb iter_100000
#bash experiments/scripts/frcnn_hinter_test.sh hinter_ZF_ch3_00 test_all_50test test_all_50train_CONVS he_rgb iter_170000
#bash experiments/scripts/frcnn_hinter_test.sh hinter_ZF_ch3_00 test_all_50test test_all_50train_ALL he_rgb iter_170000
#bash experiments/scripts/frcnn_hinter_test.sh hinter_ZF_ch4_00 test_all_50test test_all_50train he_rgb_d iter_100000
#bash experiments/scripts/frcnn_hinter_test.sh hinter_ZF_ch4_00 test_all_50test test_all_50train he_rgb_im iter_100000
#bash experiments/scripts/frcnn_hinter_test.sh hinter_ZF_ch1_00 test_all_50test test_all_50train he_d iter_100000
#bash experiments/scripts/frcnn_hinter_test.sh hinter_ZF_ch1_00 test_all_50test test_all_50train he_im iter_100000
#bash experiments/scripts/frcnn_hinter_test.sh hinter_ZF_ch3_00 test_all_50test test_all_50train he_nxyz iter_100000
#bash experiments/scripts/frcnn_hinter_test.sh hinter_ZF_ch6_00 test_all_50test test_all_50train he_rgb_nxyz iter_100000
#bash experiments/scripts/frcnn_hinter_test.sh hinter_ZF_ch1_00 test_all_50test test_all_50train he_g iter_100000

#bash experiments/scripts/frcnn_hinter_test.sh hinter_ZF_ch3_00 test_all_50test test_all_50train_FIRST he_nxyz2 iter_100000
#bash experiments/scripts/frcnn_hinter_test.sh hinter_ZF_ch4_00 test_all_50test test_all_50train_FIRST he_rgb_d iter_100000
#bash experiments/scripts/frcnn_hinter_test.sh hinter_ZF_ch4_00 test_all_50test test_all_50train_FIRST he_rgb_im iter_100000
#bash experiments/scripts/frcnn_hinter_test.sh hinter_ZF_ch4_00 test_all_50test test_all_50train_RGBMEAN he_rgb_im iter_100000
bash experiments/scripts/frcnn_hinter_test.sh hinter_ZF_ch1_00 test_all_50test test_all_50train_RGBMEAN he_im iter_100000
#bash experiments/scripts/frcnn_hinter_test.sh hinter_ZF_ch6_00 test_all_50test test_all_50train_FIRST he_rgb_nxyz2 iter_100000


python ~/transdataform/tless/evaluation.py \
    ~/data/frcnn/output/hinter_ZF_ch1_00_he_im/test_all_50train_RGBMEAN/test_all_50test_iter_100000 \
    --dataset hinter
#    ~/data/frcnn/output/hinter_ZF_ch3_00_he_nxyz2/test_all_50train_FIRST/test_all_50test_iter_100000 \
#    ~/data/frcnn/output/hinter_ZF_ch4_00_he_rgb_im/test_all_50train_RGBMEAN/test_all_50test_iter_100000 \
#    ~/data/frcnn/output/hinter_ZF_ch1_00_he_im/test_all_50train_FIRST/test_all_50test_iter_100000 \
#    ~/data/frcnn/output/hinter_ZF_ch4_00_he_rgb_im/test_all_50train_FIRST/test_all_50test_iter_100000 \
#    ~/data/frcnn/output/hinter_ZF_ch6_00_he_rgb_nxyz2/test_all_50train_FIRST/test_all_50test_iter_100000 \
#    ~/data/frcnn/output/hinter_ZF_ch1_00_he_g/test_all_50train_FIRST/test_all_50test_iter_100000 \
#    ~/data/frcnn/output/hinter_ZF_ch3_00_he_rgb/test_all_50train_FIRST/test_all_50test_iter_100000 \
#    ~/data/frcnn/output/hinter_ZF_ch1_00_he_d/test_all_50train_FIRST/test_all_50test_iter_100000 \
#    ~/data/frcnn/output/hinter_ZF_ch4_00_he_rgb_d/test_all_50train_FIRST/test_all_50test_iter_100000 \


#python ~/transdataform/frcnn/plot_pr.py \
#    ~/data/frcnn/output/hinter_ZF_ch3_00_he_rgb/plot_pr_netinit_summary.png \
#    ~/data/frcnn/output/hinter_ZF_ch3_00_he_rgb/test_all_50train_ZFv2/test_all_50test_iter_100000 \
#    ~/data/frcnn/output/hinter_ZF_ch3_00_he_rgb/test_all_50train/test_all_50test_iter_100000 \
#    --labels \
#    "rgb all imagenet" \
#    "rgb first normal" \
#    "rgb all normal"


#python ~/transdataform/frcnn/plot_pr.py \
#    ~/data/frcnn/output/hinter_ZF_ch3_00_he_rgb/plot_pr_netinit_summary.png \
#    ~/data/frcnn/output/hinter_ZF_ch3_00_he_rgb/test_all_50train_ZFv2/test_all_50test_iter_100000 \
#    ~/data/frcnn/output/hinter_ZF_ch3_00_he_rgb/test_all_50train_FIRST_orig/test_all_50test_iter_100000 \
#    ~/data/frcnn/output/hinter_ZF_ch3_00_he_rgb/test_all_50train_FIRST/test_all_50test_iter_170000 \
#    --labels zf firsto first


#python ~/transdataform/frcnn/plot_pr.py \
#    ~/data/frcnn/output/plot_pr_hinter_test_first_channel_summary.pdf \
#    ~/data/frcnn/output/hinter_ZF_ch3_00_he_rgb/test_all_50train_FIRST/test_all_50test_iter_100000 \
#    ~/data/frcnn/output/hinter_ZF_ch1_00_he_d/test_all_50train_FIRST/test_all_50test_iter_100000 \
#    ~/data/frcnn/output/hinter_ZF_ch4_00_he_rgb_d/test_all_50train_FIRST/test_all_50test_iter_100000 \
#    ~/data/frcnn/output/hinter_ZF_ch1_00_he_im/test_all_50train_FIRST/test_all_50test_iter_100000 \
#    ~/data/frcnn/output/hinter_ZF_ch4_00_he_rgb_im/test_all_50train_FIRST/test_all_50test_iter_100000 \
#    ~/data/frcnn/output/hinter_ZF_ch3_00_he_nxyz2/test_all_50train_FIRST/test_all_50test_iter_100000 \
#    ~/data/frcnn/output/hinter_ZF_ch6_00_he_rgb_nxyz2/test_all_50train_FIRST/test_all_50test_iter_100000 \
#    ~/data/frcnn/output/hinter_ZF_ch1_00_he_g/test_all_50train_FIRST/test_all_50test_iter_100000 \
#    --groups 1 2 2 2 1 \
#    --labels \
#    rgb \
#    "raw depth" \
#    "rgb + raw depth" \
#    "filled depth" \
#    "rgb + filled depth" \
#    "normals" \
#    "rgb + normals" \
#    gray
