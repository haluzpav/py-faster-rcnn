#!/usr/bin/env bash


#python ~/transdataform/frcnn/plot_net_params.py \
#    ~/frcnn/models/tless_ZF_ch3_00/test.prototxt \
#    /mnt/datagrid/personal/haluzpav/frcnn/output/tless_ZF_ch3_00_tr_rgb/ZF.v2.caffemodel \
#    0

#python ~/transdataform/frcnn/plot_net_params.py \
#    ~/frcnn/models/tless_ZF_ch4_00/test.prototxt \
#    /mnt/datagrid/personal/haluzpav/frcnn/output/tless_ZF_ch4_00_te_rgb_im/test_all_upper_50train_RGBMEAN/iter_100000.caffemodel \
#    0

#python ~/transdataform/frcnn/plot_net_params.py \
#    ~/frcnn/models/tless_ZF_ch4_00/test.prototxt \
#    /mnt/datagrid/personal/haluzpav/frcnn/output/tless_ZF_ch4_00_te_rgb_im/test_all_upper_50train_ZFv2/iter_100000.caffemodel \
#    0

#python ~/transdataform/frcnn/plot_net_params.py \
#    ~/frcnn/models/tless_ZF_ch4_00/test.prototxt \
#    /mnt/datagrid/personal/haluzpav/frcnn/output/tless_ZF_ch4_00_te_rgb_im/test_all_upper_50train_FIRST/iter_100000.caffemodel \
#    0

#python ~/transdataform/frcnn/plot_net_params.py \
#    ~/frcnn/models/tless_ZF_ch1_00/test.prototxt \
#    /mnt/datagrid/personal/haluzpav/frcnn/output/tless_ZF_ch1_00_te_im/test_all_upper_50train_FIRST/iter_100000.caffemodel \
#    0


python ~/transdataform/frcnn/plot_losses.py \
    /mnt/datagrid/personal/haluzpav/frcnn/output/tless_ZF_ch4_00_te_rgb_im/test_all_upper_50train_RGBMEAN/train_.2017-05-21_19-30-02.log \
    /mnt/datagrid/personal/haluzpav/frcnn/output/tless_ZF_ch4_00_te_rgb_im/test_all_upper_50train_ZFv2/train_.2017-04-23_17-39-57.log \
    /mnt/datagrid/personal/haluzpav/frcnn/output/tless_ZF_ch4_00_te_rgb_im/test_all_upper_50train_FIRST/train_.2017-05-15_18-49-24.log \
    /mnt/datagrid/personal/haluzpav/frcnn/output/tless_ZF_ch4_01_te_rgb_im/test_all_upper_50train_ALL/train_.2017-05-20_21-27-33.log \
    --path_overall /mnt/datagrid/personal/haluzpav/frcnn/output/plot_loss_tless_rgb_im.pdf \
    --labels_overall \
    "\textit{\#1}" \
    "\textit{\#2}" \
    "\textit{\#3}" \
    "\textit{\#4}"



#python ~/transdataform/tless/evaluation.py \
#    ~/data/frcnn/output/tless_ZF_ch4_00_te_rgb_im/test_all_upper_50train_RGBMEAN/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch1_00_te_g/test_all_upper_50train_FIRST/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch1_00_te_d/test_all_upper_50train_FIRST/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch1_00_te_im/test_all_upper_50train_FIRST/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch3_00_te_nxyz2/test_all_upper_50train_FIRST/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch3_00_te_rgb/test_all_upper_50train_FIRST/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch4_00_te_rgb_d/test_all_upper_50train_FIRST/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch4_00_te_rgb_im/test_all_upper_50train_FIRST/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch6_00_te_rgb_nxyz2/test_all_upper_50train_FIRST/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch1_00_te_im/test_all_upper_50train_RGBMEAN/test_all_upper_50test_iter_100000 \



#python ~/transdataform/tless/evaluation.py \
#    ~/data/frcnn/output/hinter_ZF_ch4_00_he_rgb_im/test_all_50train_RGBMEAN/test_all_50test_iter_100000 \
#    --dataset hinter
#    ~/data/frcnn/output/hinter_ZF_ch1_00_he_g/test_all_50train_FIRST/test_all_50test_iter_100000 \
#    ~/data/frcnn/output/hinter_ZF_ch1_00_he_d/test_all_50train_FIRST/test_all_50test_iter_100000 \
#    ~/data/frcnn/output/hinter_ZF_ch1_00_he_im/test_all_50train_FIRST/test_all_50test_iter_100000 \
#    ~/data/frcnn/output/hinter_ZF_ch3_00_he_nxyz2/test_all_50train_FIRST/test_all_50test_iter_100000 \
#    ~/data/frcnn/output/hinter_ZF_ch3_00_he_rgb/test_all_50train_FIRST/test_all_50test_iter_100000 \
#    ~/data/frcnn/output/hinter_ZF_ch4_00_he_rgb_d/test_all_50train_FIRST/test_all_50test_iter_100000 \
#    ~/data/frcnn/output/hinter_ZF_ch4_00_he_rgb_im/test_all_50train_FIRST/test_all_50test_iter_100000 \
#    ~/data/frcnn/output/hinter_ZF_ch6_00_he_rgb_nxyz2/test_all_50train_FIRST/test_all_50test_iter_100000 \
#    ~/data/frcnn/output/hinter_ZF_ch1_00_he_im/test_all_50train_RGBMEAN/test_all_upper_50test_iter_100000 \



#python ~/transdataform/frcnn/plot_pr.py \
#    ~/data/frcnn/output/plot_pr_testall_channels_first_100k.pdf \
#    ~/data/frcnn/output/tless_ZF_ch1_00_te_g/test_all_upper_50train_FIRST/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch3_00_te_rgb/test_all_upper_50train_FIRST/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch1_00_te_d/test_all_upper_50train_FIRST/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch1_00_te_im/test_all_upper_50train_FIRST/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch3_00_te_nxyz2/test_all_upper_50train_FIRST/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch4_00_te_rgb_d/test_all_upper_50train_FIRST/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch4_00_te_rgb_im/test_all_upper_50train_FIRST/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch6_00_te_rgb_nxyz2/test_all_upper_50train_FIRST/test_all_upper_50test_iter_100000 \
#    --groups 1 2 3 4 5 3 4 5 \
#    --grouping number \
#    --labels \
#    gray \
#    RGB \
#    "raw depth" \
#    "filled depth" \
#    "normals" \
#    "RGB + raw depth" \
#    "RGB + filled depth" \
#    "RGB + normals" \



#python ~/transdataform/frcnn/plot_pr.py \
#    ~/data/frcnn/output/plot_pr_hinter_test_first_channel_summary.pdf \
#    ~/data/frcnn/output/hinter_ZF_ch1_00_he_g/test_all_50train_FIRST/test_all_50test_iter_100000 \
#    ~/data/frcnn/output/hinter_ZF_ch3_00_he_rgb/test_all_50train_FIRST/test_all_50test_iter_100000 \
#    ~/data/frcnn/output/hinter_ZF_ch1_00_he_d/test_all_50train_FIRST/test_all_50test_iter_100000 \
#    ~/data/frcnn/output/hinter_ZF_ch1_00_he_im/test_all_50train_FIRST/test_all_50test_iter_100000 \
#    ~/data/frcnn/output/hinter_ZF_ch3_00_he_nxyz2/test_all_50train_FIRST/test_all_50test_iter_100000 \
#    ~/data/frcnn/output/hinter_ZF_ch4_00_he_rgb_d/test_all_50train_FIRST/test_all_50test_iter_100000 \
#    ~/data/frcnn/output/hinter_ZF_ch4_00_he_rgb_im/test_all_50train_FIRST/test_all_50test_iter_100000 \
#    ~/data/frcnn/output/hinter_ZF_ch6_00_he_rgb_nxyz2/test_all_50train_FIRST/test_all_50test_iter_100000 \
#    --groups 1 2 3 4 5 3 4 5 \
#    --grouping number \
#    --labels \
#    gray \
#    RGB \
#    "raw depth" \
#    "filled depth" \
#    "normals" \
#    "RGB + raw depth" \
#    "RGB + filled depth" \
#    "RGB + normals" \
