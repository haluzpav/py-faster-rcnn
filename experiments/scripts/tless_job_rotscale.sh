#!/usr/bin/env bash


#python ~/transdataform/tless/transform_train.py 1.36 0
#python ~/transdataform/tless/transform_train.py 1.28 0
#python ~/transdataform/tless/transform_train.py 1.20 0
#python ~/transdataform/tless/transform_train.py 1.12 0
#python ~/transdataform/tless/transform_train.py 1.04 0
#python ~/transdataform/tless/transform_train.py 0.96 0
#python ~/transdataform/tless/transform_train.py 0.88 0
#python ~/transdataform/tless/transform_train.py 0.80 0 --mask --inpaint median --derivate sobel3_absxy
#python ~/transdataform/tless/transform_train.py 0.73 0
#python ~/transdataform/tless/transform_train.py 0.67 0
#python ~/transdataform/tless/transform_train.py 0.62 0
#python ~/transdataform/tless/transform_train.py 0.57 0
#python ~/transdataform/tless/transform_train.py 0.53 0
#python ~/transdataform/tless/transform_train.py 0.50 0
#python ~/transdataform/tless/transform_train.py 0.47 0


#python ~/transdataform/tless/lists.py train_s1.36_r000_all train_primesense_s1.36_r000
#python ~/transdataform/tless/lists.py train_s1.28_r000_all train_primesense_s1.28_r000
#python ~/transdataform/tless/lists.py train_s1.20_r000_all train_primesense_s1.20_r000
#python ~/transdataform/tless/lists.py train_s1.12_r000_all train_primesense_s1.12_r000
#python ~/transdataform/tless/lists.py train_s1.04_r000_all train_primesense_s1.04_r000
#python ~/transdataform/tless/lists.py train_s0.96_r000_all train_primesense_s0.96_r000
#python ~/transdataform/tless/lists.py train_s0.88_r000_all train_primesense_s0.88_r000
#python ~/transdataform/tless/lists.py train_s0.80_r000_all train_primesense_s0.80_r000
#python ~/transdataform/tless/lists.py train_s0.73_r000_all train_primesense_s0.73_r000
#python ~/transdataform/tless/lists.py train_s0.67_r000_all train_primesense_s0.67_r000
#python ~/transdataform/tless/lists.py train_s0.62_r000_all train_primesense_s0.62_r000
#python ~/transdataform/tless/lists.py train_s0.57_r000_all train_primesense_s0.57_r000
#python ~/transdataform/tless/lists.py train_s0.53_r000_all train_primesense_s0.53_r000
#python ~/transdataform/tless/lists.py train_s0.50_r000_all train_primesense_s0.50_r000
#python ~/transdataform/tless/lists.py train_s0.47_r000_all train_primesense_s0.47_r000


#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch3_00 train_s1.36_r000_all train_s0.80_r000_all rgb iter_100000
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch3_00 train_s1.28_r000_all train_s0.80_r000_all rgb iter_100000
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch3_00 train_s1.20_r000_all train_s0.80_r000_all rgb iter_100000
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch3_00 train_s1.12_r000_all train_s0.80_r000_all rgb iter_100000
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch3_00 train_s1.04_r000_all train_s0.80_r000_all rgb iter_100000
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch3_00 train_s0.96_r000_all train_s0.80_r000_all rgb iter_100000
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch3_00 train_s0.88_r000_all train_s0.80_r000_all rgb iter_100000
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch3_00 train_s0.80_r000_all train_s0.80_r000_all rgb iter_100000
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch3_00 train_s0.73_r000_all train_s0.80_r000_all rgb iter_100000
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch3_00 train_s0.67_r000_all train_s0.80_r000_all rgb iter_100000
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch3_00 train_s0.62_r000_all train_s0.80_r000_all rgb iter_100000
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch3_00 train_s0.57_r000_all train_s0.80_r000_all rgb iter_100000
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch3_00 train_s0.53_r000_all train_s0.80_r000_all rgb iter_100000
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch3_00 train_s0.50_r000_all train_s0.80_r000_all rgb iter_100000
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch3_00 train_s0.47_r000_all train_s0.80_r000_all rgb iter_100000


#python ~/transdataform/frcnn/plot_pr.py \
#    ~/data/frcnn/output/tless_ZF_ch3_00_rgb/train_s0.80_r000_all/plot_pr_scale_summary.pdf \
#    ~/data/frcnn/output/tless_ZF_ch3_00_rgb/train_s0.80_r000_all/train_s0.47_r000_all \
#    ~/data/frcnn/output/tless_ZF_ch3_00_rgb/train_s0.80_r000_all/train_s0.50_r000_all \
#    ~/data/frcnn/output/tless_ZF_ch3_00_rgb/train_s0.80_r000_all/train_s0.53_r000_all \
#    ~/data/frcnn/output/tless_ZF_ch3_00_rgb/train_s0.80_r000_all/train_s0.57_r000_all \
#    ~/data/frcnn/output/tless_ZF_ch3_00_rgb/train_s0.80_r000_all/train_s0.62_r000_all \
#    ~/data/frcnn/output/tless_ZF_ch3_00_rgb/train_s0.80_r000_all/train_s0.67_r000_all \
#    ~/data/frcnn/output/tless_ZF_ch3_00_rgb/train_s0.80_r000_all/train_s0.73_r000_all \
#    ~/data/frcnn/output/tless_ZF_ch3_00_rgb/train_s0.80_r000_all/train_s0.80_r000_all \
#    ~/data/frcnn/output/tless_ZF_ch3_00_rgb/train_s0.80_r000_all/train_s0.88_r000_all \
#    ~/data/frcnn/output/tless_ZF_ch3_00_rgb/train_s0.80_r000_all/train_s0.96_r000_all \
#    ~/data/frcnn/output/tless_ZF_ch3_00_rgb/train_s0.80_r000_all/train_s1.04_r000_all \
#    ~/data/frcnn/output/tless_ZF_ch3_00_rgb/train_s0.80_r000_all/train_s1.12_r000_all \
#    ~/data/frcnn/output/tless_ZF_ch3_00_rgb/train_s0.80_r000_all/train_s1.20_r000_all \
#    ~/data/frcnn/output/tless_ZF_ch3_00_rgb/train_s0.80_r000_all/train_s1.28_r000_all \
#    ~/data/frcnn/output/tless_ZF_ch3_00_rgb/train_s0.80_r000_all/train_s1.36_r000_all \
#    --groups opposite \
#    --labels \
#    "\$59\%\$" \
#    "\$63\%\$" \
#    "\$67\%\$" \
#    "\$71\%\$" \
#    "\$77\%\$" \
#    "\$83\%\$" \
#    "\$91\%\$" \
#    "\$"{100..170..10}"\%\$"





# TODO output size to train in transform
#python ~/transdataform/tless/transform_train.py 1 010 --rgb --yaml
#python ~/transdataform/tless/transform_train.py 1 020 --rgb --yaml
#python ~/transdataform/tless/transform_train.py 1 030 --rgb --yaml
#python ~/transdataform/tless/transform_train.py 1 045 --rgb --yaml
#python ~/transdataform/tless/transform_train.py 1 060 --rgb --yaml
#python ~/transdataform/tless/transform_train.py 1 090 --rgb --yaml
#python ~/transdataform/tless/transform_train.py 1 120 --rgb --yaml
#python ~/transdataform/tless/transform_train.py 1 150 --rgb --yaml
#python ~/transdataform/tless/transform_train.py 1 180 --rgb --yaml
#python ~/transdataform/tless/transform_train.py 1 330 --rgb --yaml

#python ~/transdataform/tless/lists_split.py train_s1.00_r010_all_upper train_primesense_s1.00_r010
#python ~/transdataform/tless/lists_split.py train_s1.00_r020_all_upper train_primesense_s1.00_r020
#python ~/transdataform/tless/lists_split.py train_s1.00_r030_all_upper train_primesense_s1.00_r030
#python ~/transdataform/tless/lists_split.py train_s1.00_r045_all_upper train_primesense_s1.00_r045
#python ~/transdataform/tless/lists_split.py train_s1.00_r060_all_upper train_primesense_s1.00_r060
#python ~/transdataform/tless/lists_split.py train_s1.00_r090_all_upper train_primesense_s1.00_r090
#python ~/transdataform/tless/lists_split.py train_s1.00_r120_all_upper train_primesense_s1.00_r120
#python ~/transdataform/tless/lists_split.py train_s1.00_r150_all_upper train_primesense_s1.00_r150
#python ~/transdataform/tless/lists_split.py train_s1.00_r180_all_upper train_primesense_s1.00_r180
#python ~/transdataform/tless/lists_split.py train_s1.00_r330_all_upper train_primesense_s1.00_r330

#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch3_00 train_s1.00_r010_all_upper_50test train_all_upper_50train rgb iter_100000 scaletrain
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch3_00 train_s1.00_r020_all_upper_50test train_all_upper_50train rgb iter_100000 scaletrain
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch3_00 train_s1.00_r030_all_upper_50test train_all_upper_50train rgb iter_100000 scaletrain
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch3_00 train_s1.00_r045_all_upper_50test train_all_upper_50train rgb iter_100000 scaletrain
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch3_00 train_s1.00_r060_all_upper_50test train_all_upper_50train rgb iter_100000 scaletrain
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch3_00 train_s1.00_r090_all_upper_50test train_all_upper_50train rgb iter_100000 scaletrain
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch3_00 train_s1.00_r120_all_upper_50test train_all_upper_50train rgb iter_100000 scaletrain
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch3_00 train_s1.00_r150_all_upper_50test train_all_upper_50train rgb iter_100000 scaletrain
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch3_00 train_s1.00_r180_all_upper_50test train_all_upper_50train rgb iter_100000 scaletrain
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch3_00 train_s1.00_r330_all_upper_50test train_all_upper_50train rgb iter_100000 scaletrain

#python ~/transdataform/tless/evaluation.py \
#    ~/data/frcnn/output/tless_ZF_ch3_00_rgb/train_all_upper_50train/train_s1.00_r010_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch3_00_rgb/train_all_upper_50train/train_s1.00_r020_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch3_00_rgb/train_all_upper_50train/train_s1.00_r030_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch3_00_rgb/train_all_upper_50train/train_s1.00_r330_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch3_00_rgb/train_all_upper_50train/train_s1.00_r045_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch3_00_rgb/train_all_upper_50train/train_s1.00_r060_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch3_00_rgb/train_all_upper_50train/train_s1.00_r090_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch3_00_rgb/train_all_upper_50train/train_s1.00_r120_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch3_00_rgb/train_all_upper_50train/train_s1.00_r150_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch3_00_rgb/train_all_upper_50train/train_s1.00_r180_all_upper_50test_iter_100000

#python ~/transdataform/frcnn/plot_pr.py \
#    ~/data/frcnn/output/tless_ZF_ch3_00_rgb/train_all_upper_50train/plot_pr_rot_summary.pdf \
#    ~/data/frcnn/output/tless_ZF_ch3_00_rgb/train_all_upper_50train/train_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch3_00_rgb/train_all_upper_50train/train_s1.00_r010_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch3_00_rgb/train_all_upper_50train/train_s1.00_r020_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch3_00_rgb/train_all_upper_50train/train_s1.00_r030_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch3_00_rgb/train_all_upper_50train/train_s1.00_r330_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch3_00_rgb/train_all_upper_50train/train_s1.00_r045_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch3_00_rgb/train_all_upper_50train/train_s1.00_r060_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch3_00_rgb/train_all_upper_50train/train_s1.00_r090_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch3_00_rgb/train_all_upper_50train/train_s1.00_r120_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch3_00_rgb/train_all_upper_50train/train_s1.00_r150_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch3_00_rgb/train_all_upper_50train/train_s1.00_r180_all_upper_50test_iter_100000 \
#    --groups 1 1 1 2 1 1 1 1 1 1 \
#    --labels \
#    "\$0^\circ$" \
#    "\$10^\circ$" \
#    "\$20^\circ$" \
#    "\$30^\circ$" \
#    "\$-30^\circ$" \
#    "\$45^\circ$" \
#    "\$60^\circ$" \
#    "\$90^\circ$" \
#    "\$120^\circ$" \
#    "\$150^\circ$" \
#    "\$180^\circ$"

