#!/usr/bin/env bash


#python ~/transdataform/tless/lists_split.py test_all_upper test_primesense
#python ~/transdataform/tless/lists_split.py test_01to09_upper test_primesense --id_list {1..9}
#python ~/transdataform/tless/lists_split.py test_10to20_upper test_primesense --id_list {10..20}


#bash experiments/scripts/frcnn_tless_train.sh tless_ZF_ch3_00 test_all_upper_50train rgb tless_ZF_ch3_00_rgb/ZF.v2 100000 scaletest
#bash experiments/scripts/frcnn_tless_train.sh tless_ZF_ch3_00 test_01to09_upper_50train rgb tless_ZF_ch3_00_rgb/ZF.v2 100000 scaletest
#bash experiments/scripts/frcnn_tless_train.sh tless_ZF_ch3_00 test_10to20_upper_50train rgb tless_ZF_ch3_00_rgb/ZF.v2 100000 scaletest


#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch3_00 test_all_upper_50test test_all_upper_50train rgb iter_100000 scaletest --rpn
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch3_00 test_01to09_upper_50test test_01to09_upper_50train rgb iter_100000 scaletest
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch3_00 test_10to20_upper_50test test_10to20_upper_50train rgb iter_100000 scaletest
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch3_00 test_all_upper_50test test_01to09_upper_50train rgb iter_100000 scaletest
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch3_00 test_all_upper_50test test_10to20_upper_50train rgb iter_100000 scaletest
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch3_00 test_01to09_upper_50test test_all_upper_50train rgb iter_100000 scaletest
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch3_00 test_01to09_upper_50test test_10to20_upper_50train rgb iter_100000 scaletest
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch3_00 test_10to20_upper_50test test_all_upper_50train rgb iter_100000 scaletest
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch3_00 test_10to20_upper_50test test_01to09_upper_50train rgb iter_100000 scaletest


#python ~/transdataform/tless/evaluation.py \
#    ~/data/frcnn/output/tless_ZF_ch3_00_te_rgb/test_01to09_upper_50train/test_01to09_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch3_00_te_rgb/test_01to09_upper_50train/test_10to20_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch3_00_te_rgb/test_01to09_upper_50train/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch3_00_te_rgb/test_10to20_upper_50train/test_01to09_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch3_00_te_rgb/test_10to20_upper_50train/test_10to20_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch3_00_te_rgb/test_10to20_upper_50train/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch3_00_te_rgb/test_all_upper_50train_ZFv2/test_01to09_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch3_00_te_rgb/test_all_upper_50train_ZFv2/test_10to20_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch3_00_te_rgb/test_all_upper_50train_ZFv2/test_all_upper_50test_iter_100000 \


#t1="black"
#t2="color"
#t3="all"
#python ~/transdataform/frcnn/plot_pr.py \
#    ~/data/frcnn/output/tless_ZF_ch3_00_te_rgb/plot_pr_subsets.pdf \
#    ~/data/frcnn/output/tless_ZF_ch3_00_te_rgb/test_01to09_upper_50train/test_01to09_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch3_00_te_rgb/test_01to09_upper_50train/test_10to20_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch3_00_te_rgb/test_01to09_upper_50train/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch3_00_te_rgb/test_10to20_upper_50train/test_01to09_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch3_00_te_rgb/test_10to20_upper_50train/test_10to20_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch3_00_te_rgb/test_10to20_upper_50train/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch3_00_te_rgb/test_all_upper_50train_ZFv2/test_01to09_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch3_00_te_rgb/test_all_upper_50train_ZFv2/test_10to20_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch3_00_te_rgb/test_all_upper_50train_ZFv2/test_all_upper_50test_iter_100000 \
#    --groups 3 3 3 \
#    --legend_outside \
#    --labels \
#    "${t1} / ${t1}" \
#    "${t1} / ${t2}" \
#    "${t1} / ${t3}" \
#    "${t2} / ${t1}" \
#    "${t2} / ${t2}" \
#    "${t2} / ${t3}" \
#    "${t3} / ${t1}" \
#    "${t3} / ${t2}" \
#    "${t3} / ${t3}"





#bash experiments/scripts/frcnn_tless_train.sh tless_ZF_ch6_00 test_all_upper_50train rgb_nxyz tless_ZF_ch6_00_rgb_rgb/imagenet_normal 100000 scaletest
#bash experiments/scripts/frcnn_tless_train.sh tless_ZF_ch6_00 test_01to09_upper_50train rgb_nxyz tless_ZF_ch6_00_rgb_rgb/imagenet_normal 100000 scaletest
#bash experiments/scripts/frcnn_tless_train.sh tless_ZF_ch6_00 test_10to20_upper_50train rgb_nxyz tless_ZF_ch6_00_rgb_rgb/imagenet_normal 100000 scaletest


#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch6_00 test_all_upper_50test test_all_upper_50train rgb_nxyz iter_100000 scaletest
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch6_00 test_01to09_upper_50test test_01to09_upper_50train rgb_nxyz iter_100000 scaletest
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch6_00 test_10to20_upper_50test test_10to20_upper_50train rgb_nxyz iter_100000 scaletest
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch6_00 test_all_upper_50test test_01to09_upper_50train rgb_nxyz iter_100000 scaletest
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch6_00 test_all_upper_50test test_10to20_upper_50train rgb_nxyz iter_100000 scaletest
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch6_00 test_01to09_upper_50test test_all_upper_50train rgb_nxyz iter_100000 scaletest
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch6_00 test_01to09_upper_50test test_10to20_upper_50train rgb_nxyz iter_100000 scaletest
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch6_00 test_10to20_upper_50test test_all_upper_50train rgb_nxyz iter_100000 scaletest
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch6_00 test_10to20_upper_50test test_01to09_upper_50train rgb_nxyz iter_100000 scaletest


#python ~/transdataform/tless/evaluation.py \
#    ~/data/frcnn/output/tless_ZF_ch6_00_rgb_nxyz/test_10to20_upper_50train/test_10to20_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch6_00_rgb_nxyz/test_01to09_upper_50train/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch6_00_rgb_nxyz/test_10to20_upper_50train/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch6_00_rgb_nxyz/test_all_upper_50train/test_01to09_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch6_00_rgb_nxyz/test_10to20_upper_50train/test_01to09_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch6_00_rgb_nxyz/test_all_upper_50train/test_10to20_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch6_00_rgb_nxyz/test_01to09_upper_50train/test_10to20_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch6_00_rgb_nxyz/test_all_upper_50train/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch6_00_rgb_nxyz/test_01to09_upper_50train/test_01to09_upper_50test_iter_100000


#python ~/transdataform/frcnn/plot_pr.py \
#    ~/data/frcnn/output/tless_ZF_ch6_00_rgb_nxyz/plot_pr_subsets.png \
#    ~/data/frcnn/output/tless_ZF_ch6_00_rgb_nxyz/test_01to09_upper_50train/test_01to09_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch6_00_rgb_nxyz/test_01to09_upper_50train/test_10to20_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch6_00_rgb_nxyz/test_01to09_upper_50train/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch6_00_rgb_nxyz/test_10to20_upper_50train/test_01to09_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch6_00_rgb_nxyz/test_10to20_upper_50train/test_10to20_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch6_00_rgb_nxyz/test_10to20_upper_50train/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch6_00_rgb_nxyz/test_all_upper_50train/test_01to09_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch6_00_rgb_nxyz/test_all_upper_50train/test_10to20_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch6_00_rgb_nxyz/test_all_upper_50train/test_all_upper_50test_iter_100000 \
#    --groups 3 3 3





#python ~/transdataform/tless/extend_depth.py test_primesense --mask --inpaint median --derivate sobel3_absxy
#python ~/transdataform/tless/extend_depth.py test_primesense --derivate normals_xyz2 --thresholds_normals 1050000 300


#bash experiments/scripts/frcnn_tless_train.sh tless_ZF_ch3_00 test_all_upper_50train nxyz tless_ZF_ch3_00_nxyz/imagenet_normal 100000 scaletest
#bash experiments/scripts/frcnn_tless_train.sh tless_ZF_ch3_00 test_all_upper_50train nxyz tless_ZF_ch3_00_rgb/ZF.v2 100000 scaletest
#bash experiments/scripts/frcnn_tless_train.sh tless_ZF_ch1_00 test_all_upper_50train d tless_ZF_ch1_00_d/imagenet_normal 100000 scaletest
#bash experiments/scripts/frcnn_tless_train.sh tless_ZF_ch1_00 test_all_upper_50train im tless_ZF_ch1_00_d/imagenet_normal 100000 scaletest
#bash experiments/scripts/frcnn_tless_train.sh tless_ZF_ch4_00 test_all_upper_50train rgb_im tless_ZF_ch4_00_rgb_d/imagenet_depth_normal 100000 scaletest
#bash experiments/scripts/frcnn_tless_train.sh tless_ZF_ch4_00 test_all_upper_50train rgb_d tless_ZF_ch4_00_rgb_d/imagenet_depth_normal 100000 scaletest
#bash experiments/scripts/frcnn_tless_train.sh tless_ZF_ch4_00 test_all_upper_50train rgb_d tless_ZF_ch4_00_rgb_d/imagenet_normal_rgbtoo 100000 scaletest
#bash experiments/scripts/frcnn_tless_train.sh tless_ZF_ch4_00 test_all_upper_50train rgb_sob3axy tless_ZF_ch4_00_rgb_d/imagenet_depth_normal 100000 scaletest
#bash experiments/scripts/frcnn_tless_train.sh tless_ZF_ch6_00 test_all_upper_50train rgb_nxyz tless_ZF_ch6_00_rgb_rgb/imagenet_normal 100000 scaletest


#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch1_00 test_all_upper_50test test_all_upper_50train d iter_100000 scaletest
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch1_00 test_all_upper_50test test_all_upper_50train im iter_100000 scaletest
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch3_00 test_all_upper_50test test_all_upper_50train te_nxyz iter_100000 scaletest
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch4_00 test_all_upper_50test test_all_upper_50train rgb_d iter_100000 scaletest
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch4_00 test_all_upper_50test test_all_upper_50train rgb_d iter_100000 scaletest
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch4_00 test_all_upper_50test test_all_upper_50train rgb_im iter_100000 scaletest
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch4_00 test_all_upper_50test test_all_upper_50train rgb_sob3axy iter_100000 scaletest
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch6_00 test_all_upper_50test test_all_upper_50train rgb_nxyz iter_100000 scaletest


#python ~/transdataform/tless/evaluation.py \
#    ~/data/frcnn/output/tless_ZF_ch3_00_te_nxyz/test_all_upper_50train/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch1_00_te_im/test_all_upper_50train/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch1_00_te_d/test_all_upper_50train/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch6_00_te_rgb_nxyz/test_all_upper_50train/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch4_00_te_rgb_d/test_all_upper_50train/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch4_00_te_rgb_im/test_all_upper_50train/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch4_00_te_rgb_sob3axy/test_all_upper_50train/test_all_upper_50test_iter_100000


#python ~/transdataform/frcnn/plot_pr.py \
#    ~/data/frcnn/output/plot_pr_testall_channels_100k.png \
#    ~/data/frcnn/output/tless_ZF_ch3_00_te_rgb/test_all_upper_50train/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch1_00_te_d/test_all_upper_50train/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch4_00_te_rgb_d/test_all_upper_50train/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch1_00_te_im/test_all_upper_50train/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch4_00_te_rgb_im/test_all_upper_50train/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch3_00_te_nxyz/test_all_upper_50train/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch6_00_te_rgb_nxyz/test_all_upper_50train/test_all_upper_50test_iter_100000 \
#    --groups 1 2 2 2 \
#    --labels \
#    rgb \
#    "raw depth" \
#    "rgb + raw depth" \
#    "inpainted depth" \
#    "rgb + inpainted depth" \
#    "normals" \
#    "rgb + normals" \
#    ~/data/frcnn/output/tless_ZF_ch4_00_te_rgb_sob3axy/test_all_upper_50train/test_all_upper_50test_iter_100000 \






#python ~/transdataform/frcnn/extend_net.py normal \
#    ~/frcnn/models/tless_ZF_ch3_00/test.prototxt \
#    ~/frcnn/models/tless_ZF_ch6_00/test.prototxt \
#    ~/data/frcnn/output/tless_ZF_ch3_00_tr_rgb/ZF.v2.caffemodel \
#    ~/data/frcnn/output/tless_ZF_ch6_00_te_rgb_nxyz/imagenet_normal_zfv2.caffemodel


#python ~/transdataform/frcnn/switch_net.py normal \
#    ~/frcnn/models/tless_ZF_ch3_00/test.prototxt \
#    ~/frcnn/models/tless_ZF_ch3_00/test.prototxt \
#    ~/data/frcnn/output/tless_ZF_ch3_00_rgb/ZF.v2.caffemodel \
#    ~/data/frcnn/output/tless_ZF_ch3_00_nxyz/imagenet_normal.caffemodel


#python ~/transdataform/frcnn/switch_net.py normal \
#    ~/frcnn/models/tless_ZF_ch3_00/test.prototxt \
#    ~/frcnn/models/tless_ZF_ch3_00/test.prototxt \
#    ~/data/frcnn/output/tless_ZF_ch3_00_tr_rgb/ZF.v2.caffemodel \
#    ~/data/frcnn/output/tless_ZF_ch3_00_tr_rgb/imagenet_normal_first.caffemodel \
#    first


#python ~/transdataform/frcnn/switch_net.py normal \
#    ~/frcnn/models/tless_ZF_ch3_00/test.prototxt \
#    ~/frcnn/models/tless_ZF_ch6_00/test.prototxt \
#    ~/data/frcnn/output/tless_ZF_ch3_00_tr_rgb/ZF.v2.caffemodel \
#    ~/data/frcnn/output/tless_ZF_ch6_00_te_rgb_nxyz/imagenet_normal_first.caffemodel \
#    first


#python ~/transdataform/frcnn/switch_net.py normal \
#    ~/frcnn/models/tless_ZF_ch3_00/test.prototxt \
#    ~/frcnn/models/tless_ZF_ch4_00/test.prototxt \
#    ~/data/frcnn/output/tless_ZF_ch3_00_tr_rgb/ZF.v2.caffemodel \
#    ~/data/frcnn/output/tless_ZF_ch4_00_tr_rgb_d/imagenet_normal_convs.caffemodel \
#    convs






#bash experiments/scripts/frcnn_tless_train.sh tless_ZF_ch3_00 test_all_upper_50train te_rgb tless_ZF_ch3_00_tr_rgb/imagenet_normal_first 100000 scaletest
#mv ~/data/frcnn/output/tless_ZF_ch3_00_te_rgb/test_all_upper_50train ~/data/frcnn/output/tless_ZF_ch3_00_te_rgb/test_all_upper_50train_FIRST

#bash experiments/scripts/frcnn_tless_train.sh tless_ZF_ch4_00 test_all_upper_50train te_rgb_d tless_ZF_ch4_00_tr_rgb_d/imagenet_normal_first 100000 scaletest
#mv ~/data/frcnn/output/tless_ZF_ch4_00_te_rgb_d/test_all_upper_50train ~/data/frcnn/output/tless_ZF_ch4_00_te_rgb_d/test_all_upper_50train_FIRST
#bash experiments/scripts/frcnn_tless_train.sh tless_ZF_ch4_00 test_all_upper_50train te_rgb_d tless_ZF_ch4_00_tr_rgb_d/imagenet_normal_convs 100000 scaletest
#mv ~/data/frcnn/output/tless_ZF_ch4_00_te_rgb_d/test_all_upper_50train ~/data/frcnn/output/tless_ZF_ch4_00_te_rgb_d/test_all_upper_50train_CONVS

#bash experiments/scripts/frcnn_tless_train.sh tless_ZF_ch1_00 test_all_upper_50train te_im tless_ZF_ch1_00_te_d/imagenet_rgbmean 100000 scaletest
#mv ~/data/frcnn/output/tless_ZF_ch1_00_te_im/test_all_upper_50train ~/data/frcnn/output/tless_ZF_ch1_00_te_im/test_all_upper_50train_RGBMEAN
#bash experiments/scripts/frcnn_tless_train.sh tless_ZF_ch4_00 test_all_upper_50train te_rgb_im tless_ZF_ch4_00_tr_rgb_d/imagenet_rgbmean 100000 scaletest
#mv ~/data/frcnn/output/tless_ZF_ch4_00_te_rgb_im/test_all_upper_50train ~/data/frcnn/output/tless_ZF_ch4_00_te_rgb_im/test_all_upper_50train_RGBMEAN

#bash experiments/scripts/frcnn_tless_train.sh tless_ZF_ch3_00 test_all_upper_50train te_nxyz2 tless_ZF_ch3_00_tr_rgb/imagenet_normal_first 100000 scaletest
#mv ~/data/frcnn/output/tless_ZF_ch3_00_te_nxyz2/test_all_upper_50train ~/data/frcnn/output/tless_ZF_ch3_00_te_nxyz2/test_all_upper_50train_FIRST
#bash experiments/scripts/frcnn_tless_train.sh tless_ZF_ch6_00 test_all_upper_50train te_rgb_nxyz2 tless_ZF_ch6_00_te_rgb_nxyz/imagenet_normal_first 100000 scaletest
#mv ~/data/frcnn/output/tless_ZF_ch6_00_te_rgb_nxyz2/test_all_upper_50train ~/data/frcnn/output/tless_ZF_ch6_00_te_rgb_nxyz2/test_all_upper_50train_FIRST
#bash experiments/scripts/frcnn_tless_train.sh tless_ZF_ch6_00 test_all_upper_50train te_rgb_nxyz2 tless_ZF_ch6_00_te_rgb_nxyz/imagenet_normal_zfv2 100000 scaletest
#mv ~/data/frcnn/output/tless_ZF_ch6_00_te_rgb_nxyz2/test_all_upper_50train ~/data/frcnn/output/tless_ZF_ch6_00_te_rgb_nxyz2/test_all_upper_50train_ZFv2

#bash experiments/scripts/frcnn_tless_train.sh tless_ZF_ch1_00 test_all_upper_50train te_g tless_ZF_ch1_00_te_d/imagenet_normal 100000 scaletest
#mv ~/data/frcnn/output/tless_ZF_ch1_00_te_g/test_all_upper_50train ~/data/frcnn/output/tless_ZF_ch1_00_te_g/test_all_upper_50train_FIRST


#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch3_00 test_all_upper_50test test_all_upper_50train_FIRST te_rgb iter_100000 scaletest

#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch4_00 test_all_upper_50test test_all_upper_50train_FIRST te_rgb_d iter_100000 scaletest
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch4_00 test_all_upper_50test test_all_upper_50train_CONVS te_rgb_d iter_100000 scaletest

#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch1_00 test_all_upper_50test test_all_upper_50train_RGBMEAN te_im iter_100000 scaletest
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch4_00 test_all_upper_50test test_all_upper_50train_RGBMEAN te_rgb_im iter_100000 scaletest

#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch3_00 test_all_upper_50test test_all_upper_50train_FIRST te_nxyz2 iter_100000 scaletest
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch6_00 test_all_upper_50test test_all_upper_50train_FIRST te_rgb_nxyz2 iter_100000 scaletest
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch6_00 test_all_upper_50test test_all_upper_50train_ZFv2 te_rgb_nxyz2 iter_100000 scaletest

#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch1_00 test_all_upper_50test test_all_upper_50train_FIRST te_g iter_100000 scaletest


#python ~/transdataform/tless/evaluation.py \
#    ~/data/frcnn/output/tless_ZF_ch1_00_te_im/test_all_upper_50train_RGBMEAN/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch4_00_te_rgb_im/test_all_upper_50train_RGBMEAN/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch1_00_te_g/test_all_upper_50train_FIRST/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch3_00_te_nxyz2/test_all_upper_50train_FIRST/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch6_00_te_rgb_nxyz2/test_all_upper_50train_FIRST/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch6_00_te_rgb_nxyz2/test_all_upper_50train_ZFv2/test_all_upper_50test_iter_100000


#python ~/transdataform/frcnn/plot_pr.py \
#    ~/data/frcnn/output/plot_pr_nxyz2_netinits.png \
#    ~/data/frcnn/output/tless_ZF_ch3_00_te_nxyz2/test_all_upper_50train_FIRST/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch6_00_te_rgb_nxyz2/test_all_upper_50train_FIRST/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch6_00_te_rgb_nxyz2/test_all_upper_50train_ZFv2/test_all_upper_50test_iter_100000 \
#    --labels first_nxyz2 first_rgb_nxyz2 zfv2_rgb_nxyz2


#python ~/transdataform/frcnn/plot_pr.py \
#    ~/data/frcnn/output/tless_ZF_ch4_00_te_rgb_d/plot_pr_netinits.png \
#    ~/data/frcnn/output/tless_ZF_ch4_00_te_rgb_d/test_all_upper_50train_ZFv2/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch4_00_te_rgb_d/test_all_upper_50train_FIRST/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch4_00_te_rgb_d/test_all_upper_50train_CONVS/test_all_upper_50test_iter_100000 \
#    --labels zfv2 first convs


#python ~/transdataform/frcnn/plot_pr.py \
#    ~/data/frcnn/output/plot_pr_testall_channels_first_100k.pdf \
#    ~/data/frcnn/output/tless_ZF_ch3_00_te_rgb/test_all_upper_50train_FIRST/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch1_00_te_d/test_all_upper_50train/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch4_00_te_rgb_d/test_all_upper_50train_FIRST/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch1_00_te_im/test_all_upper_50train/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch4_00_te_rgb_im/test_all_upper_50train_FIRST/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch3_00_te_nxyz2/test_all_upper_50train_FIRST/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch6_00_te_rgb_nxyz2/test_all_upper_50train_FIRST/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch1_00_te_g/test_all_upper_50train_FIRST/test_all_upper_50test_iter_100000 \
#    --groups 1 2 2 2 1 \
#    --labels \
#    rgb \
#    "raw depth" \
#    "rgb + raw depth" \
#    "filled depth" \
#    "rgb + filled depth" \
#    "normals" \
#    "rgb + normals" \
#    gray
#    ~/data/frcnn/output/tless_ZF_ch4_00_te_rgb_sob3axy/test_all_upper_50train/test_all_upper_50test_iter_100000 \



#bash experiments/scripts/frcnn_tless_train.sh tless_ZF_ch4_01 test_all_upper_50train te_rgb_im tless_ZF_ch4_00_tr_rgb_d/imagenet_normal_all 280000 scaletest
#mv ~/data/frcnn/output/tless_ZF_ch4_01_te_rgb_d/test_all_upper_50train ~/data/frcnn/output/tless_ZF_ch4_01_te_rgb_d/test_all_upper_50train_ALL
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch4_01 test_all_upper_50test test_all_upper_50train_ALL te_rgb_d iter_250000 scaletest





#python ~/transdataform/tless/evaluation.py \
#    ~/data/frcnn/output/tless_ZF_ch3_00_te_rgb/test_all_upper_50train_ZFv2/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch3_00_te_rgb/test_all_upper_50train_FIRST/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch4_00_te_rgb_im/test_all_upper_50train_RGBMEAN/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch4_00_te_rgb_im/test_all_upper_50train_ZFv2/test_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch4_00_te_rgb_im/test_all_upper_50train_FIRST/test_all_upper_50test_iter_100000 \

#python ~/transdataform/tless/evaluation.py \
#    ~/data/frcnn/output/hinter_ZF_ch3_00_he_rgb/test_all_50train_ZFv2/test_all_50test_iter_100000 \
#    ~/data/frcnn/output/hinter_ZF_ch3_00_he_rgb/test_all_50train_FIRST/test_all_50test_iter_100000 \
#    ~/data/frcnn/output/hinter_ZF_ch4_00_he_rgb_im/test_all_50train_ZFv2/test_all_50test_iter_100000 \
#    ~/data/frcnn/output/hinter_ZF_ch4_00_he_rgb_im/test_all_50train_FIRST/test_all_50test_iter_100000 \
#    --dataset hinter


python ~/transdataform/frcnn/plot_pr.py \
    ~/data/frcnn/output/plot_pr_inits_rgb.pdf \
    ~/data/frcnn/output/tless_ZF_ch3_00_te_rgb/test_all_upper_50train_ZFv2/test_all_upper_50test_iter_100000 \
    ~/data/frcnn/output/tless_ZF_ch3_00_te_rgb/test_all_upper_50train_FIRST/test_all_upper_50test_iter_100000 \
    ~/data/frcnn/output/hinter_ZF_ch3_00_he_rgb/test_all_50train_ZFv2/test_all_50test_iter_100000 \
    ~/data/frcnn/output/hinter_ZF_ch3_00_he_rgb/test_all_50train_FIRST/test_all_50test_iter_100000 \
    --groups 2 2 \
    --labels \
    "\textit{T-LESS test (\#1 / \#2)}" \
    "\textit{T-LESS test (\#3)}" \
    "\textit{LINEMOD test (\#1 / \#2)}" \
    "\textit{LINEMOD test (\#3)}" \


python ~/transdataform/frcnn/plot_pr.py \
    ~/data/frcnn/output/plot_pr_inits_rgb_im.pdf \
    ~/data/frcnn/output/tless_ZF_ch4_00_te_rgb_im/test_all_upper_50train_RGBMEAN/test_all_upper_50test_iter_100000 \
    ~/data/frcnn/output/tless_ZF_ch4_00_te_rgb_im/test_all_upper_50train_ZFv2/test_all_upper_50test_iter_100000 \
    ~/data/frcnn/output/tless_ZF_ch4_00_te_rgb_im/test_all_upper_50train_FIRST/test_all_upper_50test_iter_100000 \
    ~/data/frcnn/output/hinter_ZF_ch4_00_he_rgb_im/test_all_50train_RGBMEAN/test_all_50test_iter_100000 \
    ~/data/frcnn/output/hinter_ZF_ch4_00_he_rgb_im/test_all_50train_ZFv2/test_all_50test_iter_100000 \
    ~/data/frcnn/output/hinter_ZF_ch4_00_he_rgb_im/test_all_50train_FIRST/test_all_50test_iter_100000 \
    --groups 3 3 \
    --labels \
    "\textit{T-LESS test (\#1)}" \
    "\textit{T-LESS test (\#2)}" \
    "\textit{T-LESS test (\#3)}" \
    "\textit{LINEMOD test (\#1)}" \
    "\textit{LINEMOD test (\#2)}" \
    "\textit{LINEMOD test (\#3)}" \

