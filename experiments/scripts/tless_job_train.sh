#!/usr/bin/env bash


#bash experiments/scripts/frcnn_tless_train.sh tless_ZF_ch4_00 train_s0.80_r000_all rgb_im tless_ZF_ch4_00_rgb_d/imagenet_depth_normal 70000
#bash experiments/scripts/frcnn_tless_train.sh tless_ZF_ch4_00 train_s0.80_r000_all rgb_sob3axy tless_ZF_ch4_00_rgb_d/imagenet_depth_normal 70000
#bash experiments/scripts/frcnn_tless_train.sh tless_ZF_ch5_00 train_s0.80_r000_all rgb_d_m tless_ZF_ch5_00_rgb_d_m/imagenet_depth_normal 70000


#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch4_00 test_all_upper train_s0.80_r000_all rgb_im iter_70000
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch4_00 test_all_upper train_s0.80_r000_all rgb_sob3axy iter_70000
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch5_00 test_all_upper train_s0.80_r000_all rgb_d_m iter_70000


#python ~/transdataform/tless/extend_depth.py train_primesense --ids {8..30} --mask --inpaint median --derivate sobel3_absxy


#bash experiments/scripts/frcnn_tless_train.sh tless_ZF_ch3_00 train_all_upper_50train rgb tless_ZF_ch3_00_rgb/ZF.v2 100000 scaletrain
#bash experiments/scripts/frcnn_tless_train.sh tless_ZF_ch4_00 train_all_upper_50train rgb_m tless_ZF_ch4_00_rgb_d/imagenet_depth_normal 100000 scaletrain
#bash experiments/scripts/frcnn_tless_train.sh tless_ZF_ch4_00 train_all_upper_50train rgb_d tless_ZF_ch4_00_rgb_d/imagenet_depth_normal 100000 scaletrain
#bash experiments/scripts/frcnn_tless_train.sh tless_ZF_ch4_00 train_all_upper_50train rgb_im tless_ZF_ch4_00_rgb_d/imagenet_depth_normal 100000 scaletrain
#bash experiments/scripts/frcnn_tless_train.sh tless_ZF_ch4_00 train_all_upper_50train rgb_sob3axy tless_ZF_ch4_00_rgb_d/imagenet_depth_normal 100000 scaletrain
#bash experiments/scripts/frcnn_tless_train.sh tless_ZF_ch5_00 train_all_upper_50train rgb_d_m tless_ZF_ch5_00_rgb_d_m/imagenet_depth_normal 100000 scaletrain
#bash experiments/scripts/frcnn_tless_train.sh tless_ZF_ch5_00 train_all_upper_50train rgb_im_sob3axy tless_ZF_ch5_00_rgb_d_m/imagenet_depth_normal 100000 scaletrain
#bash experiments/scripts/frcnn_tless_train.sh tless_ZF_ch5_00 train_all_upper_50train rgb_sob3axy_sob3axy tless_ZF_ch5_00_rgb_d_m/imagenet_depth_normal 100000 scaletrain
#bash experiments/scripts/frcnn_tless_train.sh tless_ZF_ch6_00 train_all_upper_50train rgb_rgb tless_ZF_ch6_00_rgb_rgb/imagenet_copy 100000 scaletrain


#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch3_00 train_all_upper_50test train_all_upper_50train rgb iter_100000 scaletrain
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch4_00 train_all_upper_50test train_all_upper_50train rgb_m iter_100000 scaletrain
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch4_00 train_all_upper_50test train_all_upper_50train rgb_d iter_100000 scaletrain
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch4_00 train_all_upper_50test train_all_upper_50train rgb_im iter_100000 scaletrain
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch4_00 train_all_upper_50test train_all_upper_50train rgb_sob3axy iter_100000 scaletrain
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch5_00 train_all_upper_50test train_all_upper_50train rgb_d_m iter_100000 scaletrain
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch5_00 train_all_upper_50test train_all_upper_50train rgb_im_sob3axy iter_100000 scaletrain
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch5_00 train_all_upper_50test train_all_upper_50train rgb_sob3axy_sob3axy iter_100000 scaletrain
#bash experiments/scripts/frcnn_tless_test.sh tless_ZF_ch6_00 train_all_upper_50test train_all_upper_50train rgb_rgb iter_100000 scaletrain


#python ~/transdataform/tless/evaluation.py \
#    ~/data/frcnn/output/tless_ZF_ch3_00_rgb/train_all_upper_50train/train_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch4_00_rgb_m/train_all_upper_50train/train_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch4_00_rgb_d/train_all_upper_50train/train_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch4_00_rgb_im/train_all_upper_50train/train_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch4_00_rgb_sob3axy/train_all_upper_50train/train_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch5_00_rgb_d_m/train_all_upper_50train/train_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch5_00_rgb_im_sob3axy/train_all_upper_50train/train_all_upper_50test_iter_100000
#python ~/transdataform/tless/evaluation.py \
#    ~/data/frcnn/output/tless_ZF_ch5_00_rgb_sob3axy_sob3axy/train_all_upper_50train/train_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch6_00_rgb_rgb/train_all_upper_50train/train_all_upper_50test_iter_100000


#python ~/transdataform/frcnn/plot_pr.py \
#    ~/data/frcnn/output/plot_pr_channels_100k.png \
#    ~/data/frcnn/output/tless_ZF_ch3_00_rgb/train_all_upper_50train/train_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch4_00_rgb_m/train_all_upper_50train/train_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch4_00_rgb_d/train_all_upper_50train/train_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch4_00_rgb_im/train_all_upper_50train/train_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch4_00_rgb_sob3axy/train_all_upper_50train/train_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch5_00_rgb_d_m/train_all_upper_50train/train_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch5_00_rgb_im_sob3axy/train_all_upper_50train/train_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch5_00_rgb_sob3axy_sob3axy/train_all_upper_50train/train_all_upper_50test_iter_100000 \
#    ~/data/frcnn/output/tless_ZF_ch6_00_rgb_rgb/train_all_upper_50train/train_all_upper_50test_iter_100000 \
#    --groups 1 4 3 1
