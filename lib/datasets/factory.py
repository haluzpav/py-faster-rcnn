# --------------------------------------------------------
# Fast R-CNN
# Copyright (c) 2015 Microsoft
# Licensed under The MIT License [see LICENSE for details]
# Written by Ross Girshick
# --------------------------------------------------------

"""Factory method for easily getting imdbs by name."""

__sets = {}

from datasets.pascal_voc import pascal_voc
from datasets.coco import coco
from datasets.tless import TLess

# Set up voc_<year>_<split> using selective search "fast" mode
for year in ['2007', '2012']:
    for split in ['train', 'val', 'trainval', 'test']:
        name = 'voc_{}_{}'.format(year, split)
        __sets[name] = (lambda split=split, year=year: pascal_voc(split, year))

# Set up coco_2014_<split>
for year in ['2014']:
    for split in ['train', 'val', 'minival', 'valminusminival']:
        name = 'coco_{}_{}'.format(year, split)
        __sets[name] = (lambda split=split, year=year: coco(split, year))

# Set up coco_2015_<split>
for year in ['2015']:
    for split in ['test', 'test-dev']:
        name = 'coco_{}_{}'.format(year, split)
        __sets[name] = (lambda split=split, year=year: coco(split, year))

def get_imdb(name):
    """Get an imdb (image database) by name."""
    if name.startswith('t-less'):
        # because fuck this lambda shit
        return TLess('_'.join(name.split('_')[2:]))
    elif name.startswith('hinter'):
        return TLess('_'.join(name.split('_')[1:]), '/mnt/datagrid/personal/haluzpav/hinter')
    elif not __sets.has_key(name):
        raise KeyError('Unknown dataset: {}'.format(name))
    return __sets[name]()

def list_imdbs():
    """List all registered imdbs."""
    return __sets.keys()


class ImgType:
    def __init__(self, d, t):
        self.dataset = d
        self.type = t

    @staticmethod
    def decode(s):
        # te_rgb_d_im_m_gsx_...
        d = {
            'rgb': 'rgb',
            'g': 'grayscale',
            'd': 'depth',
            'im': 'depth_inpaint_median',
            'm': 'depth_mask',
            'sob3axy': 'depth_sobel3_absxy',
            'nxyz': 'depth_normals_xyz',
            'nxyz2': 'depth_normals_xyz2',  # same as nxyz but fixed, keep previous just in case
        }

        split = s.split('_')

        # hr_rgb... to mark dataset (hack for buried normalization in minibatch.py)
        # tless train, tless test, hinter train, hinter test
        assert split[0] in ('tr', 'te', 'hr', 'he'), 'specifying dataset type is mandatory'
        prefix, split = split[0], split[1:]

        return [ImgType(prefix, d[n]) for n in split]
