import os
import numpy as np
import scipy.sparse
import yaml
from cached_property import cached_property
from datasets.imdb import imdb
from fast_rcnn.config import cfg


class TLess(imdb):
    def __init__(self, image_set, path=None):
        imdb.__init__(self, image_set)
        self._image_set = image_set
        self._path = path or self.default_path
        self._classes = self._get_classes()
        self._class_to_ind = dict(zip(self.classes, range(self.num_classes)))
        self._image_paths = self._load_image_set_paths()
        self._image_index = self._init_image_index()

        self.config = {
            'min_size': 2,
        }

        assert os.path.exists(self._path), 'Path does not exist: {}'.format(self._path)

    def _get_classes(self):
        path = os.path.join(self._path, 'train')
        if not os.path.exists(path):
            path += '_primesense'
        classes = ['__background__']
        for c in os.listdir(path):
            try:
                int(c)
                classes.append(c)
            except ValueError:
                continue
        return tuple(classes)

    def image_path_at(self, i):
        """
        Return the absolute path to image i in the image sequence.
        """
        path = self._image_paths[i]
        assert os.path.exists(path), 'Path does not exist: {}'.format(path)
        return path

    def _load_image_set_paths(self):
        """
        Load the indexes listed in this dataset's image set file.
        """
        # Example path to image set file:
        # self._devkit_path + /VOCdevkit2007/VOC2007/ImageSets/Main/val.txt
        image_set_file = os.path.join(self._path, 'lists', self._image_set + '.txt')
        assert os.path.exists(image_set_file), \
            'Path does not exist: {}'.format(image_set_file)
        with open(image_set_file) as f:
            image_paths = [x.strip() for x in f.readlines()]
        return image_paths

    def _init_image_index(self):
        index = []
        for p in self._image_paths:
            i = os.path.sep.join(p.split(os.path.sep)[-5:])
            i = os.path.splitext(i)[0]
            i = i.replace(os.path.sep, '_')
            index.append(i)
        return index

    def append_flipped_images(self):
        imdb.append_flipped_images(self)
        self._image_paths *= 2

    @property
    def default_path(self):
        return os.path.join(cfg.DATA_DIR, '..', 'tless', 't-less_v2')

    def default_roidb(self):
        raise NotImplementedError

    def gt_roidb(self):
        """
        Return the database of ground-truth regions of interest. Legacy method to not break other code.
        """
        return self.annotation

    @cached_property
    def annotation(self):
        """
        Actually loads the ground-truth annotations. Watch out, when called from gt_roidb,
        more keys might be added to each image.
        """
        annotations = []
        path_group = None
        id_group = None

        for path in self._image_paths:

            # assuming _image_paths is sorted
            path_group_new = os.path.abspath(os.path.join(path, '..', '..'))  # e.g. 'obj_01'
            if path_group is None or path_group != path_group_new:
                path_group = path_group_new
                id_group = int(os.path.split(path_group)[1])
                with open(os.path.join(path_group, 'gt.yml')) as f:
                    data = yaml.load(f, Loader=yaml.CLoader)

            i = int(os.path.splitext(os.path.split(path)[1])[0])

            assert not id_group is None, 'blame some gamma rays for this'

            n_obj = len(data[i])
            boxes = np.zeros((n_obj, 4), dtype=np.uint16)
            gt_classes = np.zeros(n_obj, dtype=np.int32)
            overlaps = np.zeros((n_obj, self.num_classes), dtype=np.float32)
            # "Seg" area for pascal is just the box area
            seg_areas = np.zeros(n_obj, dtype=np.float32)

            # Load object bounding boxes into a data frame.
            for ix, obj in enumerate(data[i]):
                bb = obj['obj_bb']
                x1 = bb[0]
                y1 = bb[1]
                x2 = bb[0] + bb[2]
                y2 = bb[1] + bb[3]
                cls = obj['obj_id']
                boxes[ix, :] = [x1, y1, x2, y2]
                gt_classes[ix] = cls
                overlaps[ix, cls] = 1.0
                seg_areas[ix] = (x2 - x1 + 1) * (y2 - y1 + 1)

            overlaps = scipy.sparse.csr_matrix(overlaps)

            annotations.append({
                'boxes': boxes,
                'gt_classes': gt_classes,
                'gt_overlaps': overlaps,
                'flipped': False,
                'seg_areas': seg_areas
            })

        return annotations

    def get_annotation(self, i_img):
        return self.annotation[i_img]['gt_classes'], self.annotation[i_img]['boxes']

    def evaluate_detections(self, all_boxes, output_dir):
        # previous implementation was replaced by evaluate_image and finish_evaluation, which are used differently
        raise NotImplementedError


def main():
    raise NotImplementedError

    # from datasets.pascal_voc import pascal_voc
    #
    # d = pascal_voc('trainval', '2007')
    # res = d.roidb
    # from IPython import embed
    #
    # embed()


if __name__ == '__main__':
    main()
