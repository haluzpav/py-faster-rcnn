# --------------------------------------------------------
# Fast R-CNN
# Copyright (c) 2015 Microsoft
# Licensed under The MIT License [see LICENSE for details]
# Written by Ross Girshick
# --------------------------------------------------------

from __future__ import print_function

"""Test a Fast R-CNN network on an imdb (image database)."""

from fast_rcnn.config import cfg
from fast_rcnn.bbox_transform import clip_boxes, bbox_transform_inv
from utils.timer import Timer
import numpy as np
import cv2
from fast_rcnn.nms_wrapper import nms
import cPickle
from utils.blob import im_list_to_blob
import os
from rpn.proposal_layer import ProposalLayer
from roi_data_layer.minibatch import normalize_image


def _get_image_blob(imgs_in):
    """Converts an image into a network input.

    Arguments:
        imgs (list): list of images

    Returns:
        blob (ndarray): a data blob holding an image pyramid
        scales (list): list of image scales (relative to im) used
            in the image pyramid
    """

    imgs = []
    for img_type, img in imgs_in:
        if img is not None:
            img = normalize_image(img, img_type)
        imgs.append(img)

    scales = []

    # for size_target in cfg.TEST.SCALES:
    #     scale = float(size_target) / float(size_img_min)
    #     # Prevent the biggest axis from being more than MAX_SIZE
    #     if np.round(scale * size_img_max) > cfg.TEST.MAX_SIZE:
    #         scale = float(cfg.TEST.MAX_SIZE) / float(size_img_max)
    #     scales.append(scale)
    #     imgs.append(cv2.resize(img, None, None, fx=scale, fy=scale, interpolation=cv2.INTER_LINEAR))
    #     if img_depth is not None:
    #         imgs_depth.append(cv2.resize(img_depth, None, None, fx=scale, fy=scale, interpolation=cv2.INTER_LINEAR))
    scales.append(1)

    # Create a blob to hold the input images
    blob = im_list_to_blob([imgs])

    return blob, np.array(scales)


def _get_rois_blob(im_rois, im_scale_factors):
    """Converts RoIs into network inputs.

    Arguments:
        im_rois (ndarray): R x 4 matrix of RoIs in original image coordinates
        im_scale_factors (list): scale factors as returned by _get_image_blob

    Returns:
        blob (ndarray): R x 5 matrix of RoIs in the image pyramid
    """
    rois, levels = _project_im_rois(im_rois, im_scale_factors)
    rois_blob = np.hstack((levels, rois))
    return rois_blob.astype(np.float32, copy=False)


def _project_im_rois(im_rois, scales):
    """Project image RoIs into the image pyramid built by _get_image_blob.

    Arguments:
        im_rois (ndarray): R x 4 matrix of RoIs in original image coordinates
        scales (list): scale factors as returned by _get_image_blob

    Returns:
        rois (ndarray): R x 4 matrix of projected RoI coordinates
        levels (list): image pyramid levels used by each projected RoI
    """
    im_rois = im_rois.astype(np.float, copy=False)

    if len(scales) > 1:
        widths = im_rois[:, 2] - im_rois[:, 0] + 1
        heights = im_rois[:, 3] - im_rois[:, 1] + 1

        areas = widths * heights
        scaled_areas = areas[:, np.newaxis] * (scales[np.newaxis, :] ** 2)
        diff_areas = np.abs(scaled_areas - 224 * 224)
        levels = diff_areas.argmin(axis=1)[:, np.newaxis]
    else:
        levels = np.zeros((im_rois.shape[0], 1), dtype=np.int)

    rois = im_rois * scales[levels]

    return rois, levels


def _get_blobs(ims, rois):
    """Convert an image and RoIs within that image into network inputs."""
    blobs = {'data': None, 'rois': None}
    blobs['data'], im_scale_factors = _get_image_blob(ims)
    if not cfg.TEST.HAS_RPN:
        blobs['rois'] = _get_rois_blob(rois, im_scale_factors)
    return blobs, im_scale_factors


def im_detect(net, ims, boxes=None):
    """Detect object classes in an image given object proposals.

    Arguments:
        net (caffe.Net): Fast R-CNN network to use
        ims (ndarray): list of images to test
        boxes (ndarray): R x 4 array of object proposals or None (for RPN)

    Returns:
        scores (ndarray): R x K array of object class scores (K includes
            background as object category 0)
        boxes (ndarray): R x (4*K) array of predicted bounding boxes
    """
    blobs, im_scales = _get_blobs(ims, boxes)

    # When mapping from image ROIs to feature map ROIs, there's some aliasing
    # (some distinct image ROIs get mapped to the same feature ROI).
    # Here, we identify duplicate feature ROIs, so we only compute features
    # on the unique subset.
    if cfg.DEDUP_BOXES > 0 and not cfg.TEST.HAS_RPN:
        v = np.array([1, 1e3, 1e6, 1e9, 1e12])
        hashes = np.round(blobs['rois'] * cfg.DEDUP_BOXES).dot(v)
        _, index, inv_index = np.unique(hashes, return_index=True,
                                        return_inverse=True)
        blobs['rois'] = blobs['rois'][index, :]
        boxes = boxes[index, :]

    if cfg.TEST.HAS_RPN:
        im_blob = blobs['data']
        blobs['im_info'] = np.array(
            [[im_blob.shape[2], im_blob.shape[3], im_scales[0]]],
            dtype=np.float32)

    # reshape network inputs
    net.blobs['data'].reshape(*blobs['data'].shape)
    if cfg.TEST.HAS_RPN:
        net.blobs['im_info'].reshape(*blobs['im_info'].shape)
    else:
        net.blobs['rois'].reshape(*blobs['rois'].shape)

    # do forward
    forward_kwargs = {'data': blobs['data'].astype(np.float32, copy=False)}
    if cfg.TEST.HAS_RPN:
        forward_kwargs['im_info'] = blobs['im_info'].astype(np.float32, copy=False)
    else:
        forward_kwargs['rois'] = blobs['rois'].astype(np.float32, copy=False)
    blobs_out = net.forward(**forward_kwargs)

    if cfg.TEST.HAS_RPN:
        assert len(im_scales) == 1, "Only single-image batch implemented"
        rois = net.blobs['rois'].data.copy()
        # unscale back to raw image space
        boxes = rois[:, 1:5] / im_scales[0]

    if cfg.TEST.SVM:
        # use the raw scores before softmax under the assumption they
        # were trained as linear SVMs
        scores = net.blobs['cls_score'].data
    else:
        # use softmax estimated probabilities
        scores = blobs_out['cls_prob']

    if cfg.TEST.BBOX_REG:
        # Apply bounding-box regression deltas
        box_deltas = blobs_out['bbox_pred']
        pred_boxes = bbox_transform_inv(boxes, box_deltas)
        pred_boxes = clip_boxes(pred_boxes, next(img for it, img in ims if img is not None).shape)
    else:
        # Simply repeat the boxes, once for each class
        pred_boxes = np.tile(boxes, (1, scores.shape[1]))

    if cfg.DEDUP_BOXES > 0 and not cfg.TEST.HAS_RPN:
        # Map scores and predictions back to the original set of boxes
        # noinspection PyUnboundLocalVariable
        scores = scores[inv_index, :]
        pred_boxes = pred_boxes[inv_index, :]

    return scores, pred_boxes


def apply_nms(all_boxes, thresh):
    """Apply non-maximum suppression to all predicted boxes output by the
    test_net method.
    """
    num_classes = len(all_boxes)
    num_images = len(all_boxes[0])
    nms_boxes = [[[] for _ in range(num_images)]
                 for _ in range(num_classes)]
    for cls_ind in range(num_classes):
        for im_ind in range(num_images):
            dets = all_boxes[cls_ind][im_ind]
            # noinspection PySimplifyBooleanCheck
            if dets == []:
                continue
            # CPU NMS is much faster than GPU NMS when the number of boxes
            # is relative small (e.g., < 10k)
            # TODO(rbg): autotune NMS dispatch
            keep = nms(dets, thresh, force_cpu=True)
            if len(keep) == 0:
                continue
            nms_boxes[cls_ind][im_ind] = dets[keep, :].copy()
    return nms_boxes


def test_net(net, imdb, output_dir, limit=100, img_types=None, rpn=False):
    """Test a Fast R-CNN network on an image database."""

    from datasets.tless import TLess
    assert isinstance(imdb, TLess), 'sorry, only t-less from now on'

    num_images = len(imdb.image_index)
    n_max_digits = int(np.ceil(np.log10(num_images)))
    dict_det = {}
    dict_rpn = {}

    # timers
    _t = {'im_detect': Timer(), 'misc': Timer()}

    for i_img in range(num_images):

        imgs = []
        for it in img_types:
            path = imdb.image_path_at(i_img).replace(os.sep + 'rgb' + os.sep, os.sep + it.type + os.sep)
            if not os.path.exists(path):
                raise RuntimeError('need this file ' + path)
            else:
                img = cv2.imread(path, cv2.IMREAD_UNCHANGED)
            imgs.append((it, img))

        _t['im_detect'].tic()
        score, bbox = im_detect(net, imgs, None)
        _t['im_detect'].toc()

        _t['misc'].tic()

        if rpn:
            layer_rois = next(l for l in net.layers if isinstance(l, ProposalLayer))
            dict_rpn[i_img] = {
                'score': layer_rois._scores,
                'bbox': layer_rois._bboxes,
            }

        # kick out background (cls=0), reshape
        cls = np.tile(np.arange(1, score.shape[1], dtype=np.uint8), score.shape[0])
        score = score[:, 1:].flatten()
        bbox = bbox[:, 4:].reshape((-1, 4))

        # desc sort by score, take only the best
        i_keep = np.argsort(score)[::-1][:limit]
        cls = cls[i_keep]
        score = score[i_keep]
        bbox = bbox[i_keep]

        dict_det[i_img] = {
            'cls': cls,
            'score': score,
            'bbox': bbox,
        }

        _t['misc'].toc()

        print('im_detect: {:{}d}/{:d} {:.3f}s {:.3f}s'.format(
            i_img + 1, n_max_digits, num_images, _t['im_detect'].average_time, _t['misc'].average_time
        ))

    print('Saving detections')

    if rpn:
        with open(os.path.join(output_dir, 'rpn.pkl'), 'w') as f:
            cPickle.dump(dict_rpn, f)

    with open(os.path.join(output_dir, 'detections.pkl'), 'w') as f:
        cPickle.dump(dict_det, f)
