# --------------------------------------------------------
# Fast R-CNN
# Copyright (c) 2015 Microsoft
# Licensed under The MIT License [see LICENSE for details]
# Written by Ross Girshick
# --------------------------------------------------------

"""Blob helper functions."""

import numpy as np
import cv2


def im_list_to_blob(imgss):
    """Convert a list of images into a network input.

    Assumes images are already prepared (means subtracted, BGR order, ...).
    """
    shape = imgss[0][0].shape
    n_channels = [(1 if img is None or len(img.shape) == 2 else img.shape[2]) for img in imgss[0]]
    blob = np.zeros((len(imgss), shape[0], shape[1], sum(n_channels)), np.float32)
    i_channel_free = 0
    for i_img, imgs in enumerate(imgss):
        for i_type, img in enumerate(imgs):
            if img is not None:
                if len(img.shape) == 2:
                    img = img[..., np.newaxis]
                blob[i_img, :img.shape[0], :img.shape[1], i_channel_free:i_channel_free + n_channels[i_type]] = img
            i_channel_free += n_channels[i_type]
    # Move channels (axis 3) to axis 1
    # Axis order will become: (batch elem, channel, height, width)
    channel_swap = (0, 3, 1, 2)
    blob = blob.transpose(channel_swap)
    return blob


def prep_im_for_blob(img, pixel_means, size_target, size_max):
    """Mean subtract and scale an image for use in a blob."""
    img = img.astype(np.float32, copy=False)
    img -= pixel_means
    # useless, tless has all the sizes same
    # shape_img = img.shape
    # size_min_img = np.min(shape_img[0:2])
    # size_max_img = np.max(shape_img[0:2])
    # scale_img = float(size_target) / float(size_min_img)
    # # Prevent the biggest axis from being more than MAX_SIZE
    # if np.round(scale_img * size_max_img) > size_max:
    #     scale_img = float(size_max) / float(size_max_img)
    # img = cv2.resize(img, None, None, fx=scale_img, fy=scale_img,
    #                  interpolation=cv2.INTER_LINEAR)
    scale_img = 1

    return img, scale_img
